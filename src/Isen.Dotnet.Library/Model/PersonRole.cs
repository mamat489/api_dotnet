namespace Isen.Dotnet.Library.Model 
{
    public class PersonRole : BaseEntity
    {
        public Role Role { get; set; }
        public int RoleId { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}