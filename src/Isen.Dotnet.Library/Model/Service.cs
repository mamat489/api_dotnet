namespace Isen.Dotnet.Library.Model
{
    public class Service : BaseEntity
    {
        public string ServiceName { get;set; }
    }
}