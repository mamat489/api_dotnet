using System.Diagnostics.CodeAnalysis;
using Isen.Dotnet.Library.Model;
using Microsoft.EntityFrameworkCore;

namespace Isen.Dotnet.Library.Context
{
    public class ApplicationDbContext : DbContext
    {        
        // Listes des classes modèle / tables
        public DbSet<Person> PersonCollection { get; set; }
        public DbSet<Service> ServiceCollection { get; set; }
        public DbSet<Role> RoleCollection { get; set; }

        public ApplicationDbContext(
            [NotNullAttribute] DbContextOptions options) : 
            base(options) {  }

        protected override void OnModelCreating(
            ModelBuilder modelBuilder){        
        
        modelBuilder
                .Entity<Person>()
                .ToTable(nameof(Person))
                .HasKey(pers => pers.Id);

        modelBuilder
                .Entity<Person>()
                .HasOne(pers => pers.Service)
                .WithMany()
                .HasForeignKey(pers => pers.ServiceId);

        //Add many to many relation
        modelBuilder
                .Entity<PersonRole>()
                .ToTable(nameof(PersonRole))
                .HasKey(persr => new {  persr.PersonId, persr.Id });

        modelBuilder
                .Entity<PersonRole>()
                .HasOne(persr => persr.Person)
                .WithMany(pers => pers.PersonRoles)
                .HasForeignKey(persr => persr.PersonId);  

        modelBuilder
                .Entity<PersonRole>()
                .HasOne(persr => persr.Role)
                .WithMany(r => r.PersonRoles)
                .HasForeignKey(persr => persr.Id);


            }
        }

    }