using System;
using System.Collections.Generic;
using System.Linq;
using Isen.Dotnet.Library.Context;
using Isen.Dotnet.Library.Model;
using Microsoft.Extensions.Logging;

namespace Isen.Dotnet.Library.Services
{
    public class DataInitializer : IDataInitializer
    {
        // Générateur aléatoire
        private readonly Random _random;

        // DI de ApplicationDbContext
        private readonly ApplicationDbContext _context;
        private readonly ILogger<DataInitializer> _logger;
        public DataInitializer(
            ILogger<DataInitializer> logger,
            ApplicationDbContext context)
        {
            _context = context;
            _logger = logger;
            _random = new Random();
        }

        public List<Service> GetServices()
        {
            return new List<Service>
            {
                new Service { ServiceName = "Developpement"},
                new Service { ServiceName = "Marketing"},
                new Service { ServiceName = "Production"},
                new Service { ServiceName = "Communication"},
                new Service { ServiceName = "Commerce"}
            };
        }

        // 1 Services
        private Service Get1Service(int a)
        {
                var service = _context.ServiceCollection.ToList();
                return service[a];
        }


        public List<Role> GetRoles()
        {
            return new List<Role>
            {
                new Role { RoleName = "Developpeur"},
                new Role { RoleName = "Administrateur" },
                new Role { RoleName = "SuperAdministrateur"},
                new Role { RoleName = "User" },
                new Role { RoleName = "Manager" }

            };
        }

                // 1 role
        private Role Get1Role(int a)
        {
                var role = _context.RoleCollection.ToList();
                return role[a];
        }

        // Get back person list       
        public List<Person> GetPersons()
        {
            var persons = new List<Person>();
            for (var i = 0; i < _firstNames.Count; i++)
            {
                persons.Add(RandomPerson);
            }
            return persons;
        }

        // return number as phone number
        private string RandomPhoneNumber => 
            $"{_random.Next(10000000, 99999999)}";

        // return sample service
        private Service RandomService
        {
            get
            {
                var services = _context.ServiceCollection.ToList();
                return services[_random.Next(services.Count)];
            }
        }

        // return random role
        private Role RandomRole
        {
            get {
                var roles = _context.RoleCollection.ToList();
                return roles[_random.Next(roles.Count)];
            }
        }

        // return sample person
        private Person RandomPerson
        {
            get {
                var service = RandomService;
                var role = RandomRole;
                var firstName = RandomFirstName;
                var lastName = RandomLastName;
                var personVAR = new Person()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DateOfBirth = RandomDate,
                    PhoneNumber = RandomPhoneNumber,
                    Service = service,
                    ServiceId = service.Id,
                    PersonRoles = new List<PersonRole>()
                };
                var personRole = new PersonRole {
                    Person = personVAR,
                    PersonId = personVAR.Id,
                    Role = role,
                    RoleId = role.Id
                };
                personVAR.PersonRoles.Add(personRole);

                return personVAR;
            }
        }

        private List<string> _firstNames => new List<string>
        {
            "mat", "lui", "moi", "toi", "juien"
        };
        private List<string> _lastNames => new List<string>
        {
            "jean", "val", "toto", "henry", "doctor"
        };


        public void DropDatabase()
        {
            _logger.LogWarning("Dropping database");
            _context.Database.EnsureDeleted();
        }
            
        private string RandomFirstName =>
            _firstNames[_random.Next(_firstNames.Count)];

         //data random
        private DateTime RandomDate =>
            new DateTime(_random.Next(1960, 2000), 1, 1)
                .AddDays(_random.Next(0, 365));

        private string RandomLastName =>
            _lastNames[_random.Next(_lastNames.Count)];



        public void CreateDatabase()
        {
            _logger.LogWarning("Creating database");
            _context.Database.EnsureCreated();
        }

        public void AddPersons()
        {
            _logger.LogWarning("Adding persons...");
            // Si la base n'est pas vide -> ne rien faire
            if (_context.PersonCollection.Any()) return;
            // Générer X person
            var persons = GetPersons();
            _context.AddRange(persons);
            _context.SaveChanges();
        }

        public void AddServices()
        {
            _logger.LogWarning("Adding services");
            if (_context.ServiceCollection.Any()) return;
            var services = GetServices();
            _context.AddRange(services);
            _context.SaveChanges();
        }


        public void AddRoles()
        {
            _logger.LogWarning("Adding roles...");
            if (_context.RoleCollection.Any()) return;
            var roles = GetRoles();
            _context.AddRange(roles);
            _context.SaveChanges(); 
        }

    }
}